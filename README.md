# CubePad
CubePad jest urządzeniem elektronicznym, który działa kontroler muzyczny MIDI. Format MIDI jest używany do przekazywania informacji pomiędzy elektronicznymi instrumentami muzycznymi. MIDI umożliwia, syntezatorom, keyboardom i podobnym urządzeniom kontrolować się nawzajem oraz wymieniać informacje między sobą.

Do grania na instrumencie potrzebny jest program muzyczny. W tym programie muzycznym odpalamy wtyczkę VST, która posiada w sobie brzmienia. Przykładem takiej wtyczki VST jest Scarbee Rickenbacker Bass, który emuluje brzmienie prawdziwej gitary basowej. 

Dzięki MIDI, możemy na jednym urządzeniu posiadać brzmienia gitary, pianina, po perkusję, i tak dalej.

Projekt został zbudowany w oparciu o płytkę Arduino i bibliotece MIDIUSB, która umożliwia przesyłanie komunikatów MIDI przy pomocy kabla USB.

Krótki filmik prezentujący działanie instrumentu:
https://www.youtube.com/watch?v=b-yBUXAEIJU

Po lewej stronie zostało załadowane brzmienie perkusji.
Po prawej stronie została załadowana wtyczka VST o brzmieniu gitary basowej.

| CubePad | Przyciski i obudowa |
|--|--|
|  ![enter image description here](https://bitbucket.org/dawidwloch/cubepad/raw/1769479f1554c46e644e33b829d3a158d0039900/zdj%C4%99cia/cubepad.jpg)| ![enter image description here](https://bitbucket.org/dawidwloch/cubepad/raw/1769479f1554c46e644e33b829d3a158d0039900/zdj%C4%99cia/przyciskiiobudowa.jpg) |

|  Płytka prototypowa|Przyciski  |
|--|--|
| ![enter image description here](https://bitbucket.org/dawidwloch/cubepad/raw/1769479f1554c46e644e33b829d3a158d0039900/zdj%C4%99cia/plytkaprototypowa.jpg) | ![enter image description here](https://bitbucket.org/dawidwloch/cubepad/raw/1769479f1554c46e644e33b829d3a158d0039900/zdj%C4%99cia/przyciski.jpg) |

