#include <Multiplexer4067.h>  /* Biblioteka dla Multipleksera */
#include <Thread.h>           /* Biblioteka Thread */
#include <ThreadController.h> /* Biblioteka Thread */
#include "MIDIUSB.h"          /* Biblioteka MIDIUSB */

/* PRZYCISKI */
const byte multiplexButtons = 5;                                          // liczba przycisków używana w multiplekserze
const byte Buttons = 11;                                                  // liczba przycisków podłączonych do wejść cufroych
const byte allButtons = multiplexButtons + Buttons;                       // łączna liczba przycisków
const byte buttonPin[Buttons] = {18, 19, 20, 16, 15, 14, 6, 7, 8, 9, 10}; // które piny są używane
const byte multiplexPin[multiplexButtons] = {12, 13, 14, 15, 11};         // które piny są uzywane w multiplekserze
int actualState[allButtons] = {0};                                        // aktualny stan pryzcisku
int previousState[allButtons] = {0};                                      // poprzedni stan przycisku

/* MIDI */
byte midiChannel = 0; // inicjalizacja kanału MIDI
byte note = 36;       // najniższy użyty dźwięk MIDI
byte cc = 1;          // komunikat kontrolny

/* MULTIPLEXER */
Multiplexer4067 mplex = Multiplexer4067(2, 3, 4, 5, A3); // konfiguracja multipleksera

/* THREADS */
ThreadController cpu; // odpowiedzialny za zarządzanie wieloma wątkami.

/* DEBOUNCE */
unsigned long lastDebounceTime = 0;
unsigned long debounceDelay = 6;

void setup()
{
  mplex.begin(); // inicjalizacja multipleksera
  for (int i = 0; i < Buttons; i++)
  {
    pinMode(buttonPin[i], INPUT_PULLUP);
  }
  pinMode(A3, INPUT_PULLUP);
}

void loop()
{
  cpu.run();
  readButtons();
}

/* READ BUTTONS */

void readButtons()
{
  for (int i = 0; i < multiplexButtons; i++)
  { // pętla dla przycisków z multipleksera
    int buttonReading = mplex.readChannel(multiplexPin[i]);
    if (buttonReading > 1000)
    {
      actualState[i] = HIGH; /* Wysoki stan */
    }
    else
    {
      actualState[i] = LOW; /* Niski stan */
    }
  }

  for (int i = 0; i < Buttons; i++)
  {                                                                // pętla dla przycisków z Arduino
    actualState[i + multiplexButtons] = digitalRead(buttonPin[i]); // przechowanie aktualnego stanu
  }

  for (int i = 0; i < allButtons; i++)
  {
    // millis() - liczenie zrealizowane jest sprzętowo, z użyciem liczników (timerów)
    if ((millis() - lastDebounceTime) > debounceDelay)
    {

      if (actualState[i] != previousState[i])
      {
        lastDebounceTime = millis();

        if (actualState[i] == LOW)
        {
          noteOn(1, note + i, 127); // Kanał 1, klawisz C, czułość dźwięku
          MidiUSB.flush();
          previousState[i] = actualState[i];
        }
        else
        {
          noteOn(1, note + i, 0); // Kanał 1, klawisz C, czułość dźwięku
          MidiUSB.flush();
          previousState[i] = actualState[i];
        }
      }
    }
  }
}

/* BIBLIOTEKA MIDIUSB */
/* Generowanie funkcji podczas naciśnięcia i opuszczenia przycisku */

void noteOn(byte channel, byte pitch, byte velocity)
{
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity)
{
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void controlChange(byte channel, byte control, byte value)
{
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}
